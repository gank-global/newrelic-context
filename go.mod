module bitbucket.org/gank-global/newrelic-context

go 1.15

require (
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible
	github.com/go-redis/redis/v8 v8.11.4
	github.com/gomodule/redigo v1.8.5 // indirect
	github.com/newrelic/go-agent v3.15.1+incompatible
	github.com/yuin/gopher-lua v0.0.0-20210529063254-f4c35e4016d9 // indirect
	gorm.io/driver/postgres v1.2.3
	gorm.io/gorm v1.22.4
)
